import React, {Component} from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import NavBar from "./components/NavBar"
import Home from './pages/home'
import Register from "./pages/register"
import Login from "./pages/login"
import Room from "./pages/room"
import ProtectedRoute from "./components/ProtectedRoute"
import {AuthProvider} from './providers/AuthProvider'
import 'bootstrap/dist/css/bootstrap.min.css'
import $ from 'jquery'
import Popper from "popper.js"
import './styles/index.scss'

export default class App extends Component
{
  constructor(props)
  {
    super(props)
    this.state = {user: null, updateUser: this.updateUser, socket: null, updateSocket: this.updateSocket}
  }

  updateUser = (user) => {
    this.setState({user})
  }

  updateSocket = (socket) => {
    this.setState({socket})
  }

  render()
  {
    return (
        <AuthProvider state={this.state}>
          <Router>
            <div className="container-fluid App">
              <NavBar/>
              <Switch>
                <Route exact path="/" component={Home}/>
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
                <ProtectedRoute path="/app" component={Room}/>
              </Switch>
            </div>
          </Router>
        </AuthProvider>
    )
  }
}