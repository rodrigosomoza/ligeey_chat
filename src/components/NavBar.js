import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import Avatar from "./Avatar"
import {AuthContext} from "../providers/AuthProvider"

export default class NavBar extends Component
{

    static contextType = AuthContext

    toBeRendered = () => {
        const {user} = this.context
        if(!user)
        {
            return (
                    <>
                        <a className="navbar-brand" href="/">
                            Ligeey Chat
                        </a>
                        <ul className="menu">
                            <li><Link to="/register">Register</Link></li>
                            <li><Link to="/login">Login</Link></li>
                        </ul>
                    </>
            )
        }
        else
        {
            return (
                    <>
                        <a className="navbar-brand" href="/">
                            <Avatar/>
                            <span className="avatar-name">Rodrigo</span>
                        </a>
                        <ul className="menu">
                            <li><Link to="/app">Chat</Link></li>
                        </ul>
                    </>
                )
        }
    }

    render()
    {
        return (
            <nav className="navbar">
                {this.toBeRendered()}
            </nav>
        )
    }
}