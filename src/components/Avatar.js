import React, {Component} from 'react'

export default class Avatar extends Component
{
    render()
    {
        return (
            <img alt="Avatar" className="img-fluid rounded-circle avatar" src="/images/avatar.png"
            />
        )
    }
}