import React, {Component} from 'react'

export default class SignIn extends Component{

    render()
    {
        return (
            <div className="container credentials-box">
                <div className="card mt-4">
                    <div className="card-body">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }

}