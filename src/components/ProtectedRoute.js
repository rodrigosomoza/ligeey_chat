import React from 'react'
import {Route, Redirect} from 'react-router-dom'
import {AuthContext} from "../providers/AuthProvider"

function ProtectedRoute({component: Component, ...rest}) {
    return (
        <AuthContext.Consumer>
            {
                ({user}) => (
                    <Route {...rest} render={props => {
                        if (!user)
                            return <Component {...props} />
                        else
                            return <Redirect to={ {
                                pathname: "/",
                                state: {
                                    from: props.location
                                }
                            } }/>
                    }} />
                )
            }
        </AuthContext.Consumer>
    )
}

export default ProtectedRoute