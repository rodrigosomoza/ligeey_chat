import React, {Component} from 'react'
import SignIn from "../components/SignIn"
import {AuthContext} from "../providers/AuthProvider"

export default class Register extends Component{

    constructor(props)
    {
        super(props)
        this.state = {name: '', email: '', number_phone: '', password: ''}
    }

    static contextType = AuthContext

    handleSubmit = (e) => {
        e.preventDefault()
        this.postUser(this.state).then((registeredUser) => {
            console.log('enregistrement reussi')
            this.setState({name: '', email: '', number_phone: '', password: ''})
        }).catch((err) => console.error(err))
    }

    postUser = async (data) => {
        let response = await fetch('http://localhost:3000/api/user/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        if(response.ok)
        {
            return await response.json()
        }
    }

    handleName = (e) => {
        this.setState({name: e.target.value})
    }

    handleEmail = (e) => {
        this.setState({email: e.target.value})
    }

    handleNumberPhone = (e) => {
        this.setState({number_phone: e.target.value})
    }

    handlePassword = (e) => {
        this.setState({password: e.target.value})
    }

    render() {
        return (
            <SignIn>
                <h4 className="card-title text-center">Registration</h4>
                <form action="" className="form-horizontal" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label htmlFor="name">Name: </label>
                        <input className="form-control" type="text" value={this.state.name} onChange={this.handleName} id="name" name='name' required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email: </label>
                        <input className="form-control" type="email" value={this.state.email} onChange={this.handleEmail} id="email" name='email' required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="number_phone">Phone: </label>
                        <input className="form-control" type="number" value={this.state.number_phone} onChange={this.handleNumberPhone} id="number_phone" name='number_phone' required/>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password: </label>
                        <input className="form-control" type="password" value={this.state.password} onChange={this.handlePassword} id="password" name='password' required/>
                    </div>
                    <button type="submit" className="btn btn-md btn-primary">Register</button>
                </form>
            </SignIn>
        )
    }

}

