const Joi = require('@hapi/joi')

// Registration Validation
const registerValidation = inputs => {
    const schema = Joi.object({
        name: Joi.string().min(4).max(255).required(),
        email: Joi.string().min(6).max(255).required().email(),
        number_phone: Joi.string().min(9).max(11).required(),
        password: Joi.string().min(6).max(1024).required()
    }).options({ abortEarly: false })
    return schema.validate(inputs)
}

// Login Validation
const loginValidation = inputs => {
    const schema = Joi.object({
        email: Joi.string().min(6).max(255).required().email(),
        password: Joi.string().min(6).max(1024).required()
    }).options({ abortEarly: false })
    return schema.validate(inputs)
}

module.exports.registerValidation = registerValidation
module.exports.loginValidation = loginValidation