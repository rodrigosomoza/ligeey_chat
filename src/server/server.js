const express = require('express')
const app = express()
const http = require('http').createServer(app)
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const cors = require('cors')

// Setting up socket.io
const io = module.exports.io = require('socket.io')(http)
const SocketManager = require('./SocketManager')
io.on('connection', SocketManager)

// Middleware
app.use(express.json())
app.use(cors({origin: 'http://localhost:8000'}))

// Setting API Routes
const authRoute = require('./routes/auth')

// Connect to mongodb
dotenv.config()
mongoose.connect(process.env.DB_CONNECT, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true,}).then(console.log('connected to db'))

// Route Middlewares
app.use('/api/user', authRoute)


app.listen(3000)