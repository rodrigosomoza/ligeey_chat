const io = require('./server').io

module.exports = function (socket) {
    console.log(`Client ${socket.id} connected`)
}