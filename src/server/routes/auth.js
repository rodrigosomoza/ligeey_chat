const router = require('express').Router()
const User = require('../model/User')
const {registerValidation, loginValidation} = require('../validations/validations')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

// Register
router.post('/register', async (req, res) => {

    // Validating data before registering
    const {error} = registerValidation(req.body)
    if(error)
    {
        let errorMessage = {}
        error.details.forEach(detail => {
            errorMessage[detail.context.key] = detail.message
        })
        return res.status(400).send(errorMessage)
    }

    // Checking if user email already exist
    const userEmailExist = await User.findOne({email: req.body.email})
    if(userEmailExist) return res.status(400).send('Email already exist')
    // Checking if user number phone already exist
    const userNumberPhoneExist = await User.findOne({number_phone: req.body.number_phone})
    if(userNumberPhoneExist) return res.status(400).send('This number phone has already been used')

    // Hashing the password
    const salt = await bcrypt.genSalt(10)
    const passwordHashed = await bcrypt.hash(req.body.password, salt)

    // Creating a new user
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        number_phone: req.body.number_phone,
        password: passwordHashed
    })
    try
    {
        const savedUser = await user.save()
        res.send({user: savedUser._id})
    }
    catch (err) {
        res.status(400).send(err)
    }
})

// Login
router.post('/login', async (req, res) => {
    // Validating user credentials
    const {error} = loginValidation(req.body)
    if(error)
    {
        let errorMessage = {}
        error.details.forEach(detail => {
            errorMessage[detail.context.key] = detail.message
        })
        return res.status(400).send(errorMessage)
    }
    // Checking if user email already exist
    const user = await User.findOne({email: req.body.email})
    if(!user) return res.status(400).send('Email does not exist')
    // Checking if password is correct
    const validPass = await bcrypt.compare(req.body.password, user.password)
    if(!validPass) return res.status(400).send('Incorrect password')
    // Creating and assigning a token
    const token = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET)
    res.header('auth-token', token).status(201).send(token)
})

module.exports = router