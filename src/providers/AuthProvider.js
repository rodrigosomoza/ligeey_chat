import React, {createContext} from 'react'

export const AuthContext = createContext({
    user: null,
    socket: null,
    updateUser: () => {},
    updateSocket: () => {}
})

export function AuthProvider(props)
{
    return (
        <>
            <AuthContext.Provider value={props.state}>
                {props.children}
            </AuthContext.Provider>
        </>
    )
}